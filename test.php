<?php

require 'drupal_twig.php';

// Example No1: Just a list.
$list = new DrupalTemplate('list', array(
  'items' => array(
    'foo',
    'bar',
  ),
));
print $list;


// Example No2: A list within a list.
$list = new DrupalTemplate('list', array(
  'items' => array(
    'foo',
    new DrupalTemplate('list', array(
      'items' => array(
        'bar',
        'baz'
      ),
    )),
  ),
));
print $list;


// Example No3: attributes.
$items = array(
  'foo',
  // This is something that passes attributes to the <li>.
  new MarkupItem('bar', array('class' => array('this', 'that'))),
  // This is a link.
  array(
    'href' => 'http://example.com',
    'title' => 'link title',
  ),
);
$list = new DrupalTemplate('list', array(
  // This is something that passes attributes to the <ul>.
  'items' => new MarkupItem($items, array('class' => 'ulclasss')),
));
print $list;

function preprocess($type, $context) {
  if ($type == 'list') {
    foreach ($context['items'] as $item) {
      if ($item instanceof MarkupItem) {
        $item->attributes['class'][] = 'preprocess';
        $item->attributes['data'][] = 'new';
      }
    }
  }
}

function drupal_get_template_variable(array $context, $name) {
  return $name . 'foo';
}
