<?php
define('DRUPAL_ROOT', getcwd());

require_once DRUPAL_ROOT . '/Twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

class Drupal_Twig_NodeVisitor implements Twig_NodeVisitorInterface {
  function enterNode(Twig_NodeInterface $node, Twig_Environment $env) {
    if (get_class($node) == 'Twig_Node_Expression_Name') {
      return new Drupal_Twig_Node_Expression_Name($node->getAttribute('name'), $node->getLine());
    }
    return $node;
  }
  function leaveNode(Twig_NodeInterface $node, Twig_Environment $env) {
    return $node;
  }
  function getPriority() {
    return 0;
  }
}
class Drupal_Twig_Node_Expression_Name extends Twig_Node_Expression_Name {
    public function compile(Twig_Compiler $compiler)
    {
        $name = $this->getAttribute('name');
        if ($this->getAttribute('is_defined_test')) {
            if ($this->isSpecial()) {
                $compiler->repr(true);
            } else {
                $compiler->raw('array_key_exists(')->repr($name)->raw(', $context)');
            }
        } elseif ($this->isSpecial()) {
            $compiler->raw($this->specialVars[$name]);
        } else {
            // remove the non-PHP 5.4 version when PHP 5.3 support is dropped
            // as the non-optimized version is just a workaround for slow ternary operator
            // when the context has a lot of variables
            if (version_compare(phpversion(), '5.4.0RC1', '>=') && ($this->getAttribute('ignore_strict_check') || !$compiler->getEnvironment()->isStrictVariables())) {
                // PHP 5.4 ternary operator performance was optimized
                $compiler
                    ->raw('isset($context[')
                    ->string($name)
                    ->raw(']) ? $context[')
                    ->string($name)
                    ->raw('] : ($context[')
                    ->string($name)
                    ->raw('] = drupal_get_template_variable($context, ')
                    ->string($name)
                    ->raw('))')
                 ;
            } else {
                $compiler
                    ->raw('$this->getDrupalContext($context, ')
                    ->string($name)
                ;

                if ($this->getAttribute('ignore_strict_check')) {
                    $compiler->raw(', true');
                }

                $compiler
                    ->raw(')')
                ;
            }
        }
    }
}

abstract class Drupal_Twig_Template extends Twig_Template {
    protected function getDrupalContext($context, $item, $ignoreStrictCheck = false)
    {
        if (!array_key_exists($item, $context)) {
          $context[$item] = drupal_get_template_variable($context, $item);
        }
        if (!array_key_exists($item, $context)) {
            if ($ignoreStrictCheck || !$this->env->isStrictVariables()) {
                return null;
            }

            throw new Twig_Error_Runtime(sprintf('Variable "%s" does not exist', $item));
        }

        return $context[$item];
    }

}

function twig($reset = TRUE) {
  static $twig;
  if (empty($twig) || $reset) {
    // We will have our own loader.
    $loader = new Twig_Loader_Filesystem(DRUPAL_ROOT . '/templates');
    $twig = new Twig_Environment($loader, array(
        'cache' => DRUPAL_ROOT . '/compilation_cache',
        'base_template_class' => 'Drupal_Twig_Template',
    ));
    $twig->addNodeVisitor(new Drupal_Twig_NodeVisitor);
    $twig->addFunction('attributes', new Twig_Function_Function('attribute_print'));
  }
  return $twig;
}

class DrupalTemplate extends Twig_Markup {
  protected $context;
  function __construct($type, $context = array()) {
    $this->type = $type;
    $this->context = $context;
  }
  function __toString() {
    try {
      preprocess($this->type, $this->context);
      $template = twig()->loadTemplate($this->type . '.html');
      $return = $template->render($this->context);
    }
    catch (Exception $e) {
      echo $e->getMessage();
    }
    return $return;
  }
}

class DrupalEscapedContent extends Twig_Markup {
  function __construct($content) {
    parent::__construct($content, 'UTF-8');
  }
}

function check_plain($string) {
  return (is_object($string) && $string instanceof Twig_Markup) ? (string) $string : htmlentities($string, ENT_QUOTES, 'UTF-8');
}

class MarkupItem implements Iterator {
  public $attributes = array();
  protected $content, $key, $value, $valid;
  function __construct($content, $attributes) {
    $this->attributes = $attributes;
    $this->content = $content;
    if (is_array($this->content)) {
      $this->rewind();
    }
  }
  function current() {
    return $this->value;
  }
  function key() {
    return $this->key;
  }
  function next() {
    $this->valid = list($this->key, $this->value) = each($this->content);
  }
  function rewind() {
    reset($this->content);
    $this->next();
  }
  function valid() {
    return $this->valid !== FALSE;
  }
  function __toString() {
    return $this->content;
  }
}

function attribute_print($markupItem) {
  if (is_object($markupItem) && isset($markupItem->attributes)) {
    $attributes = '';
    foreach ($markupItem->attributes as $key => $value) {
      $attributes .= check_plain($key) . ' = "' . check_plain(is_array($value) ? implode(' ', $value) : $value) . '" ';
    }
    // We need a variable because of the reference return.
    return new DrupalEscapedContent($attributes);
  }
}
